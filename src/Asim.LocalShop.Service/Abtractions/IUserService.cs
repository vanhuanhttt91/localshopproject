﻿using Asim.LocalShop.Shared.DTOs;
using Asim.LocalShop.Shared.Response;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Asim.LocalShop.Service.Abtractions
{
    public interface IUserService
    {
        Task InsertUserAsync(UserDto user);
        Task<UserResponse> GetUsersAsync(string id);
    }
}
