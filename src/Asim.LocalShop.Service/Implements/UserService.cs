﻿using Asim.LocalShop.Data.Repositories.Abstractions;
using Asim.LocalShop.Service.Abtractions;
using Asim.LocalShop.Shared.DTOs;
using Asim.LocalShop.Shared.Response;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Asim.LocalShop.Service.Implements
{
    public class UserSerivce : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserSerivce(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<UserResponse> GetUsersAsync(string id)
        {
            var user = await _userRepository.BuildQuery().GetByUserId(id);
            return new UserResponse
            {
                Id = id,
                UserName = user.UserName
            };

        }

        public Task InsertUserAsync(UserDto user)
        {
            throw new NotImplementedException();
        }
    }
}
