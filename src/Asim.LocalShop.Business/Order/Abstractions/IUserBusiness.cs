﻿using Asim.LocalShop.Shared.DTOs;
using Asim.LocalShop.Shared.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asim.LocalShop.Business.Implements
{
    public interface IUserBusiness
    {
        Task InsertUserAsync(UserDto user);
        Task<UserResponse> GetUsersAsync(string id);
    }
}
