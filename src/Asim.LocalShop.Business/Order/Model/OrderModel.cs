﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asim.LocalShop.Business.Order.Model
{
   public class OrderModel
    {
        public string CustomerId { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal PaidAmount { get; set; }
        public string CustomerName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string Dictrict { get; set; }
        public string Ward { get; set; }
        public string HomeNumber { get; set; }
        public string Note { get; set; }
        public string AdministrativeTime { get; set; }
    }
}
