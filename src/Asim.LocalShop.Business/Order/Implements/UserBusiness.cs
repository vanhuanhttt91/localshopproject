﻿using Asim.LocalShop.Business.Implements;
using Asim.LocalShop.Data.UnitOfWork;
using Asim.LocalShop.Service.Abtractions;
using Asim.LocalShop.Shared.DTOs;
using Asim.LocalShop.Shared.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asim.LocalShop.Business.Order.Implements
{
    public class UserBusiness : IUserBusiness
    {
        private readonly IUserService _userService;
        private readonly IUnitOfWorkService _unitOfWorkService;
        public UserBusiness(IUserService userService, IUnitOfWorkService unitOfWorkService)
        {
            _userService = userService;
            _unitOfWorkService = unitOfWorkService;
        }
        public async Task<UserResponse> GetUsersAsync(string id)
        {
            var user = await _userService.GetUsersAsync(id);
            return new UserResponse
            {
                Id = user.Id,
                UserName = user.UserName
            };
        }

        public async Task InsertUserAsync(UserDto user)
        {
            await _userService.InsertUserAsync(user);
            await _unitOfWorkService.SaveChangesAsync();
        }
    }
}
