﻿using Asim.LocalShop.Api.ViewModel;
using Asim.LocalShop.Business.Implements;
using Asim.LocalShop.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Asim.LocalShop.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserBusiness _userBusiness;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        public UserController(IUserBusiness userBusiness, UserManager<User> userManager,
                              SignInManager<User> signInManager)
        {
            _userBusiness = userBusiness;
            _userManager = userManager;
            _signInManager = signInManager;
        }
        [HttpGet]
        [Route("{userName}/{otpCode}/check-otp")]
        public async Task<IActionResult> CheckOtpCodeAsync(string userName, string otpCode)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(x => x.UserName == userName && x.OTPCode == otpCode);
            if (user is not null)
            {
                return Ok(true);

            }
            return BadRequest(false);
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> LoginAsync(LoginViewModel user)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(user.UserName, user.Password, true, false);

                if (result.Succeeded)
                {
                    return Ok(true);
                }
            }
            return BadRequest("Login Failure");
        }
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> RegisterAsync(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User
                {
                    UserName = model.UserName
                };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return Ok("Register success");
                }
                foreach (var error in result.Errors)
        {
                    ModelState.AddModelError(error.Code, error.Description);
                }

                return BadRequest(ModelState);
            }
            return BadRequest("Register failure");
        }
    }
}
