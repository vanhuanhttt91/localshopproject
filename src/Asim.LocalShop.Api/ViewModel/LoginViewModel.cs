﻿using System.ComponentModel.DataAnnotations;

namespace Asim.LocalShop.Api.ViewModel
{
    public class LoginViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
