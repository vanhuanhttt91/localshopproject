﻿using Asim.LocalShop.Data.Contexts;
using Asim.LocalShop.Data.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Asim.LocalShop.Data.UnitOfWork
{
    public class UnitOfWorkService : IUnitOfWorkService
    {
        private readonly LocalShopDbContext _dbContext;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dbContext"></param>
        public UnitOfWorkService(LocalShopDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void SaveChanges()
        {
            try
            {
                _dbContext.SaveChanges();
            }
            catch (DbUpdateConcurrencyException exception)
            {
                foreach (EntityEntry entityEntry in exception.Entries)
                {
                    entityEntry.Reload();
                }

                IEnumerable<object> entities = exception.Entries.Select(entry => entry.Entity);

                throw new DataConflictException(entities);
            }
            catch (Exception ex)
            {

            }

        }

        /// <summary>
        /// Save all data changes
        /// </summary>
        /// <returns></returns>
        public async Task SaveChangesAsync()
        {
            try
            {
                await _dbContext.SaveChangesAsync()
                    .ConfigureAwait(false);
            }
            catch (DbUpdateConcurrencyException exception)
            {
                foreach (EntityEntry entityEntry in exception.Entries)
                {
                    await entityEntry.ReloadAsync().ConfigureAwait(false);
                }

                IEnumerable<object> entities = exception.Entries.Select(entry => entry.Entity);

                throw new DataConflictException(entities);
            }
            catch (Exception ex)
            {

            }
        }
    }
}
