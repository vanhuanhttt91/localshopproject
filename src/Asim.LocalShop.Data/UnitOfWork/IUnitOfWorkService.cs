﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asim.LocalShop.Data.UnitOfWork
{
    public interface IUnitOfWorkService
    {
        /// <summary>
        /// Save all data changes
        /// </summary>
        /// <returns></returns>
        Task SaveChangesAsync();
        void SaveChanges();
    }
}
