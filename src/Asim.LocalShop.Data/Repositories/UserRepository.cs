﻿using Asim.LocalShop.Data.Contexts;
using Asim.LocalShop.Data.Models;
using Asim.LocalShop.Data.Queries;
using Asim.LocalShop.Data.Queries.Abstractions;
using Asim.LocalShop.Data.Repositories.Abstractions;
using System;

namespace Asim.LocalShop.Data.Repositories
{
    public class UserRepository : RepositoryBase<User, Guid>, IUserRepository
    {
        public UserRepository(LocalShopDbContext dbContext) : base(dbContext)
        { }
        public IUserQuery BuildQuery()
        {
            return new UserQuery(DbContext.Users.AsQueryable(), DbContext);
        }
    }
}
