﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Asim.LocalShop.Data.Repositories.Abstractions
{
    public interface IRepository<TEntity, TPrimaryKey> where TEntity : class where TPrimaryKey : struct
    {
        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        Task<TEntity> GetByIdAsync(Guid keyValue);

        /// <summary>
        /// Get by composite Id
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="keyValue2"></param>
        /// <returns></returns>
        Task<TEntity> GetByCompositeId(Guid keyValue, Guid keyValue2);

        /// <summary>
        /// Add new record
        /// </summary>
        /// <param name="newEntity"></param>
        /// <returns></returns>
        Task AddAsync(TEntity newEntity);
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="newEntity"></param>
        void Update(TEntity newEntity);

        void Delete(TEntity newEntity);


        /// <summary>
        /// Add list record
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        void AddRange(IEnumerable<TEntity> entities);
    }
}
