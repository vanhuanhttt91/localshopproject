﻿using Asim.LocalShop.Data.Models;
using Asim.LocalShop.Data.Queries.Abstractions;
using System;

namespace Asim.LocalShop.Data.Repositories.Abstractions
{
    public interface IUserRepository : IRepository<User, Guid>
    {
        /// <summary>
        /// Build IOrderQuery query
        /// </summary>
        /// <returns></returns>
        IUserQuery BuildQuery();

        //   Task<IEnumerable<FilterOrderResponse>> GetDistrictAsync();

    }
}
