﻿using Asim.LocalShop.Data.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Asim.LocalShop.Data.Repositories.Abstractions
{
    public abstract class RepositoryBase<TEntity, TPrimaryKey> : IRepository<TEntity, TPrimaryKey>
        where TEntity : class
        where TPrimaryKey : struct
    {
        /// <summary>
        /// Dbcontext
        /// </summary>
        protected LocalShopDbContext DbContext { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dbContext"></param>
        protected RepositoryBase(LocalShopDbContext dbContext)
        {
            DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        /// <summary>
        /// Add new record
        /// </summary>
        /// <param name="newEntity"></param>
        /// <returns></returns>
        public async Task AddAsync(TEntity newEntity)
        {
            if (newEntity is null)
            {
                throw new ArgumentNullException(nameof(newEntity));
            }

            await DbContext.AddAsync(newEntity).ConfigureAwait(false);
        }
        /// <summary>
        /// Add new record
        /// </summary>
        /// <param name="newEntity"></param>
        /// <returns></returns>
        public void Update(TEntity newEntity)
        {
            if (newEntity is null)
            {
                throw new ArgumentNullException(nameof(newEntity));
            }
            DbContext.Entry(newEntity).State = EntityState.Modified;
        }
        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public async Task<TEntity> GetByIdAsync(Guid keyValue)
        {
            return await DbContext.Set<TEntity>().FindAsync(keyValue).ConfigureAwait(false);
        }
        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="keyValue2"></param>
        /// <returns></returns>
        public async Task<TEntity> GetByCompositeId(Guid keyValue, Guid keyValue2)
        {
            return await DbContext.Set<TEntity>().FindAsync(keyValue, keyValue2).ConfigureAwait(false);
        }
        /// <summary>
        /// Add new record
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public void AddRange(IEnumerable<TEntity> entities)
        {
            if (entities is null)
            {
                throw new ArgumentNullException(nameof(entities));
            }
            DbContext.AddRange(entities);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newEntity"></param>
        public void Delete(TEntity newEntity)
        {
            if (newEntity is null)
            {
                throw new ArgumentNullException(nameof(newEntity));
            }
            DbContext.Set<TEntity>().Remove(newEntity);
        }
    }
}
