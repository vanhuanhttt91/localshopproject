﻿using Asim.LocalShop.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asim.LocalShop.Data.Queries.Abstractions
{
    public interface IUserQuery : IQuery<User>
    {
        /// <summary>
        /// GetDetailProgram
        /// </summary>
        /// <returns></returns>
        Task<User> GetByUserId(string id);

        /// <summary>
        /// Get user by user name
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        Task<User> GetByUserName(string userName);


    }
}
