﻿using Asim.LocalShop.Data.Contexts;
using Asim.LocalShop.Data.Models;
using Asim.LocalShop.Data.Queries.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asim.LocalShop.Data.Queries
{
    public class UserQuery : QueryBase<User>, IUserQuery
    {
        private readonly LocalShopDbContext _dbContext;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="User"></param>
        /// <param name="dbContext"></param>
        public UserQuery(IQueryable<User> User, LocalShopDbContext dbContext) : base(User)
        { _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext)); }



        /// <summary>
        /// GetByUserId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<User> GetByUserId(string id)
        {
            var user = await Query.Where(p => p.Id == id).FirstOrDefaultAsync().ConfigureAwait(true);
            if (user is null)
            {
                return new User();
            }
            return user;
        }

        public async Task<User> GetByUserName(string userName)
        {
            var user = await Query.Where(p => p.UserName == userName).FirstOrDefaultAsync().ConfigureAwait(true);
            if (user is null)
            {
                return new User();
            }
            return user;
        }
    }
}
