﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asim.LocalShop.Data.Models
{
    public class UserGroup: BaseEntity
    {
        public string Name { get; set; }
    }
}
