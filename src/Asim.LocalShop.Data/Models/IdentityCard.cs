﻿using System.ComponentModel.DataAnnotations;

namespace Asim.LocalShop.Data.Models
{
    public class IdentityCard: BaseEntity
    {
        [MaxLength(50)]
        public string IdentityCardId { get; set; }
        [MaxLength(255)]
        public string CustomerId { get; set; }
    }
}
