﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asim.LocalShop.Data.Models
{
    [Table("Order")]
    public class Order : BaseEntity
    {
        public string CustomerId { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal PaidAmount { get; set; }
        public int FormPaymentFrist { get; set; }
        public int FormPaymentSecond { get; set; }
        public int status { get; set; }
        [MaxLength(100)]
        public string CustomerName { get; set; }
        [MaxLength(100)]
        public string PhoneNumber { get; set; }
        [MaxLength(100)]
        public string Email { get; set; }
        [MaxLength(100)]
        public string City { get; set; }
        [MaxLength(100)]
        public string Dictrict { get; set; }
        [MaxLength(100)]
        public string Ward { get; set; }
        [MaxLength(100)]
        public string HomeNumber { get; set; }
        [MaxLength(int.MaxValue)]
        public string Note { get; set; }
        public bool AdministrativeTime { get; set; }
    }
}
