﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Asim.LocalShop.Data.Models
{
    public class LockNumber: BaseEntity
    {
        [MaxLength(255)]
        public string CustomerId { get; set; }
        public DateTime DateEnd { get; set; }
    }
}
