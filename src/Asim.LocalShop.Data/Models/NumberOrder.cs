﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asim.LocalShop.Data.Models
{
    public class NumberOrder: BaseEntity
    {
        public string Number { get; set; }
        public decimal PriceNumber { get; set; }
        public decimal PriceService { get; set; }
        public int Status { get; set; }
        public DateTime DealLineActive { get; set; }
        public string Serial { get; set; }
    }
}
