﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asim.LocalShop.Data.Models
{
    public class User: IdentityUser
    {
        public string OTPCode { get; set; }
    }
}
