﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;

namespace Asim.LocalShop.Data.Models
{
    public partial class BaseEntity
    {
        /// <summary>
        /// BaseEntity
        /// </summary>
        public BaseEntity()
        {
            CreatedAt = DateTime.UtcNow;
            IsActive = true;
        }
        /// <summary>
        /// Id
        /// </summary>
        [Required]
        [Key]
        public string Id { get; set; }
        /// <summary>
        /// CreatedAt
        /// </summary>
        public DateTime CreatedAt { get; set; }
        /// <summary>
        /// UpdatedAt
        /// </summary>
        public DateTime? UpdatedAt { get; set; }
        /// <summary>
        /// CreatedBy
        /// </summary>
        [MaxLength(255)]
        public string CreatedBy { get; set; }
        /// <summary>
        /// UpdatedBy
        /// </summary>
        [MaxLength(255)]
        public string UpdatedBy { get; set; }
        /// <summary>
        /// IsActive
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// IsDeleted
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}
