﻿using Asim.LocalShop.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
namespace Asim.LocalShop.Data.Contexts
{
    public class LocalShopDbContext : IdentityDbContext<User>
    {
        public LocalShopDbContext(DbContextOptions<LocalShopDbContext> options) : base(options)
        {
        }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<NumberOrder> NumberOrders { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Use Fluent API to configure
            base.OnModelCreating(modelBuilder);
            // Map entities to tables
            modelBuilder.Entity<NumberOrder>().ToTable("NumberOrder");
            modelBuilder.Entity<Order>().ToTable("Order");
            modelBuilder.Entity<IdentityCard>().ToTable("IdentityCard");
            modelBuilder.Entity<LockNumber>().ToTable("LockNumber");

            //  Configure Primary Keys

            //  Configure indexes 

            // Configure columns

            //modelBuilder.Entity<User>().Property(u => u.Id).HasColumnType("nvarchar(100)").IsRequired();
            //modelBuilder.Entity<User>().Property(u => u.FirstName).HasColumnType("nvarchar(50)");
            //modelBuilder.Entity<User>().Property(u => u.UserName).HasColumnType("nvarchar(50)");
            //modelBuilder.Entity<User>().Property(u => u.LastName).HasColumnType("nvarchar(50)");

            //Configure relationships

            //modelBuilder.Entity<User>().HasData(
            //    new User { Id = "1", UserName = "test" },
            //     new User { Id = "2", UserName = "check" });
        }
    }
}
